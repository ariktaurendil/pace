/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;
using IniParser;

class PaceListBoxOptionsTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    if ( !Gtk.init_check ( ref args ) )
    {
      return 77; //meson exit code to skip test
    }

    PaceListBoxOptionsTest.add_tests ();

    string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                          "[repo1]\nServer = value\n\n" +
                          "#[repo2]\n#Server = value\n#Include = value2";

    try {
      FileUtils.set_contents ( "pace_listbox_options_test.conf", file_content );
    } catch ( Error e ) {
      stderr.printf ( "Error creating config file (%s)\n", e.message );
    }

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/listbox_options", () => {
      ListBoxOptions test_listbox = new ListBoxOptions ();

      assert ( test_listbox is ListBoxOptions );
    } );

    Test.add_func ( "/pace/listbox_options/set_options/get_options", () => {
      bool check = true;

      ListBoxOptions test_listbox = new ListBoxOptions ();

      ConfigFile test_file = new ConfigFile ( "pace_listbox_options_test.conf" );

      test_listbox.set_options ( test_file );

      Array<Key> options = test_listbox.get_options ();

      check = check && options.index ( 0 ).to_string () == "# key = value\n";
      check = check && options.index ( 1 ).to_string () == "key2 = value2\n";
      check = check && options.index ( 2 ).to_string () == "option\n";

      assert ( check );
    } );
  }
}
