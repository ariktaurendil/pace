/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;

class PaceRepositoryFormTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    if ( !Gtk.init_check ( ref args ) )
    {
      return 77; //meson exit code to skip test
    }

    PaceRepositoryFormTest.add_tests ();

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/repository_form", () => {
      RepositoryForm test_form = new RepositoryForm ();

      assert ( test_form is RepositoryForm );
    } );

    Test.add_func ( "/pace/repository_form/get_listbox", () => {
      RepositoryForm test_form = new RepositoryForm ();

      ListBox test_listbox = test_form.get_listbox ();

      assert ( test_listbox.length == 0 );
    } );

    Test.add_func ( "/pace/repository_form/insert_row_combobox_entry", () => {
      RepositoryForm test_form = new RepositoryForm ();

      test_form.insert_row_combobox_entry ( "Server" );

      ListBox test_listbox = test_form.get_listbox ();
      RowKeyValue row = test_listbox.get_row_at_index ( 0 ).get_child () as RowKeyValue;

      assert ( row.get_key () == "Server" );
    } );

    Test.add_func ( "/pace/repository_form/insert_row_label_entry", () => {
      RepositoryForm test_form = new RepositoryForm ();

      test_form.insert_row_label_entry ( "Server" );

      ListBox test_listbox = test_form.get_listbox ();
      RowKeyValue row = test_listbox.get_row_at_index ( 0 ).get_child () as RowKeyValue;

      assert ( row.get_key () == "Server" );
    } );

    Test.add_func ( "/pace/repository_form/clear", () => {
      RepositoryForm test_form = new RepositoryForm ();

      test_form.insert_row_label_entry ( "Name" );
      test_form.insert_row_combobox_entry ( "Server" );

      test_form.clear ();

      ListBox test_listbox = test_form.get_listbox ();

      assert ( test_listbox.length == 0 );
    } );
  }
}
