/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_row_option.vala
 *
 * Copyright 2018-2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using IniParser;

namespace Pace {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/pace/ui/pace_row_option.ui" )]
  public class RowOption : RowActivatable, RowSizeGroup, RowChangeable {
    [GtkChild]
    private RowLabelEntry label_entry;
    private IniParser.KeyWithValue with_value;
    private string initial_data;

    public RowOption ( Key option ) {
      this.label_entry.set_key ( option.get_key () );

      if ( option.get_commented () == Commented.NO ) {
        this.set_enabled ( true );
      } else {
        this.set_enabled ( false );
      }

      this.with_value = option.get_with_value ();

      if ( this.with_value == KeyWithValue.YES ) {
        this.label_entry.set_value ( option.get_value () );
      } else {
        this.label_entry.entry_visible = false;
      }

      this.initial_data = this.to_config_key ().to_string ();
    }

    public bool get_changed () {
      return this.to_config_key ().to_string () != this.initial_data;
    }

    public Key to_config_key () {
      string name = this.label_entry.get_key ();
      string value = this.label_entry.get_value ();
      Commented commented = Commented.YES;
      if ( this.get_enabled () ) {
        commented = Commented.NO;
      }

      return new Key ( name, value, this.with_value, commented );
    }

    [GtkCallback]
    private void option_changed () {
      this.changed ();
    }

    public void set_size_group ( Gtk.SizeGroup size_group ) {
      this.label_entry.set_size_group ( size_group );
    }
  }
}
