/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_cli.vala
 *
 * Copyright (C) 2019-2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using IniParser;

namespace Pace {
  public class Cli {
    private ConfigFile config_file;

    public Cli ( string file ) {
      this.config_file = new ConfigFile ( file );
    }

    public int run ( string[] args ) {
      int ret = 0;

      switch ( args[1] ) {
        case "add": {
          if ( args.length > 2 ) {
            ret = this.add_repository ( args );
          }
          break;
        }
        case "remove": {
          if ( args.length > 2 ) {
            ret = this.remove_repository ( args[2] );
          }
          break;
        }
        case "move": {
          if ( args.length > 2 ) {
            ret = this.move_repository ( args );
          }
          break;
        }
        case "enable": {
          if ( args.length > 2 ) {
            ret = this.enable_repository ( args[2], true );
          }
          break;
        }
        case "disable": {
          if ( args.length > 2 ) {
            ret = this.enable_repository ( args[2], false );
          }
          break;
        }
        case "set": {
          if ( args.length > 3 ) {
            ret = this.set_option ( args[2], args[3] );
          } else {
            if ( args.length > 2 ) {
              ret = this.set_option ( args[2], "" );
            }
          }
          break;
        }
        case "unset": {
          if ( args.length > 2 ) {
            ret = this.unset_option ( args[2] );
          }
          break;
        }
        case "list": {
          this.list_repository ();
          break;
        }
        case "list-options": {
          this.list_options ();
          break;
        }
        case "--version": {
          this.print_version ();
          break;
        }
        case "-h":
        case "--help":
        default: {
          this.print_help ();
          break;
        }
      }

      if ( this.config_file.changed () ) {
        ret = this.save_file ();
      }

      return ret;
    }

    private void print_version () {
      stdout.printf ( "Pace v%s\n", Config.VERSION );
    }

    private void print_help () {
      Array<string> arguments = new Array<string> ();
      Array<string> arguments_description = new Array<string> ();

      string output = "pace [OPTION...] [COMMAND]\n\n" +
                      "Manage repositories and options in /etc/pacman.conf\n" +
                      "\n";

      arguments.append_val ( "Options:" );
      arguments_description.append_val ( "" );
      arguments.append_val ( "-h, --help" );
      arguments_description.append_val ( "show this help" );
      arguments.append_val ( "--version" );
      arguments_description.append_val ( "show package version" );
      arguments.append_val ( "" );
      arguments_description.append_val ( "" );
      arguments.append_val ( "Commands:" );
      arguments_description.append_val ( "" );
      arguments.append_val ( "enable NAME" );
      arguments_description.append_val ( "Enable NAME repository" );
      arguments.append_val ( "disable NAME" );
      arguments_description.append_val ( "Disable NAME repository" );
      arguments.append_val ( "list" );
      arguments_description.append_val ( "Show the list of repositories" );
      arguments.append_val ( "move NAME OPTION" );
      arguments_description.append_val ( "move NAME repository to OPTION" );
      arguments.append_val ( "add NAME" );
      arguments_description.append_val ( "add NAME repositorty" );
      arguments.append_val ( "list-options" );
      arguments_description.append_val ( "Show the list of options" );
      arguments.append_val ( "remove NAME" );
      arguments_description.append_val ( "remove NAME repository" );
      arguments.append_val ( "set KEY VALUE" );
      arguments_description.append_val ( "Set VALUE to KEY" );
      arguments.append_val ( "unset KEY" );
      arguments_description.append_val ( "Set use the default value in KEY" );

      var longest = this.longest_string ( arguments );

      for ( int i = 0; i < arguments.length; i++ ) {
        if ( arguments.index ( i ) != "" ) {
          output += arguments.index ( i );
          output += build_separator ( arguments.index ( i ).length, longest, 5 );
          output += arguments_description.index ( i );
        }
        output += "\n";
      }
      stdout.printf ( "%s", output );
    }

    private int enable_repository ( string repo_name, bool enable ) {
      int ret = 0;

      if ( !config_file.enable_repository ( repo_name, enable ) ) {
        ret = 1;
        stderr.printf ( "Repository '%s' does not exist\n", repo_name );
      }

      return ret;
    }

    private int remove_repository ( string repo_name ) {
      int ret = 0;

      if ( !config_file.remove_repository ( repo_name ) ) {
        ret = 1;
        stderr.printf ( "Repository '%s' does not exist\n", repo_name );
      }

      return ret;
    }

    private int add_repository ( string[] args ) {
      int ret = 0;

      var repo_name = args[2];
      if ( !this.repository_exists ( repo_name ) ) {
        string dest = "";
        CliMove place = CliMove.ABOVE;
        var new_repo = new Section ( repo_name );
        for ( int i = 3; i < args.length; i += 2 ) {
          if ( i+1 < args.length || args[i] == "--disabled" ) {
            switch ( args[i] ) {
              case "--server":
              case "--include":
              case "--siglevel":
              case "--usage": {
                var key = new Key ( this.arg_to_key ( args[i] ), args[i+1], KeyWithValue.YES );
                new_repo.add_key ( key );
                break;
              }
              case "--disabled": {
                new_repo.set_commented ( Commented.YES );
                i--;
                break;
              }
              case "--above": {
                if ( this.repository_exists ( args[i+1] ) ) {
                  dest = args[i+1];
                  place = CliMove.ABOVE;
                } else {
                  stderr.printf ( "Repository '%s' does not exist\n", dest );
                  ret = 1;
                }
                break;
              }
              case "--below": {
                if ( this.repository_exists ( args[i+1] ) ) {
                  dest = args[i+1];
                  place = CliMove.BELOW;
                } else {
                  stderr.printf ( "Repository '%s' does not exist\n", dest );
                  ret = 1;
                }
                break;
              }
            }
          } else {
            ret = 1;
            if ( args[i] == "--server" ||  args[i] == "--include" ||
                 args[i] == "--siglevel" ||  args[i] == "--usage" ) {
              stderr.printf ( "Error: Argument '%s' needs a value\n", args[i] );
            } else {
              stderr.printf ( "Error: '%s' is not a valid argument\n", args[i] );
            }
            break;
          }
        }

        if ( ret == 0 ) {
          if ( !this.config_file.add_repository ( new_repo ) ) {
            ret = 1;
          }

          if ( ret == 0 && dest != "" ) {
            this.move_repository_to ( new_repo.get_name (), this.repository_index ( dest ), place );
          }
        }
      } else {
        ret = 1;
        stderr.printf ( "Already exists a repository named '%s'\n", repo_name );
      }


      return ret;
    }

    private int move_repository ( string[] args ) {
      int ret = 0;

      var repo = args[2];
      if ( this.repository_exists ( repo ) ) {
        switch ( args[3] ) {
          case "--up": {
            ret = this.move_repository_to ( repo, this.repository_index ( repo ) - 1, CliMove.ABOVE );
            break;
          }
          case "--down": {
            ret = this.move_repository_to ( repo, this.repository_index ( repo ) + 1, CliMove.BELOW );
            break;
          }
          case "--first": {
            ret = this.move_repository_to ( repo, 0, CliMove.ABOVE );
            break;
          }
          case "--last": {
            var repositories = this.config_file.get_repositories ();
            ret = this.move_repository_to ( repo, (int)repositories.length - 1, CliMove.BELOW  );
            break;
          }
          case "--above": {
            if ( args.length > 4 ) {
              var dest = args[4];
              if ( this.repository_exists ( dest ) ) {
                ret = this.move_repository_to ( repo, this.repository_index ( dest ), CliMove.ABOVE );
              } else {
                stderr.printf ( "Repository '%s' does not exist\n", dest );
                ret = 1;
              }
            }
            break;
          }
          case "--below": {
            if ( args.length > 4 ) {
              var dest = args[4];
              if ( this.repository_exists ( dest ) ) {
                ret = this.move_repository_to ( repo, this.repository_index ( dest ), CliMove.BELOW );
              } else {
                stderr.printf ( "Repository '%s' does not exist\n", dest );
                ret = 1;
              }
            }
            break;
          }
        }
      } else {
        ret = 1;
        stderr.printf ( "Repository '%s' does not exist\n", repo );
      }

      return ret;
    }

    private int move_repository_to ( string name, int index_destination, CliMove place ) {
      int ret = 0;
      int index_origin = this.repository_index ( name );
      var repositories = this.config_file.get_repositories ();

      if ( index_origin > index_destination && place == CliMove.BELOW ) {
        index_destination++;
      }

      if ( index_origin < index_destination && place == CliMove.ABOVE ) {
        index_destination--;
      }

      if ( index_destination < 0 ) {
        index_destination = 0;
      } else {
        if ( index_destination > (repositories.length)-1 ) {
          index_destination = (int)(repositories.length-1);
        }
      }

      if ( index_destination != index_origin ) {
        var repo = repositories.index ( index_origin );
        var reordered = new Array<Section> ();

        for ( int i = 0; i < repositories.length; i++ ) {
          if ( repositories.index ( i ).get_name () != name ) {
            reordered.append_val ( repositories.index ( i ) );
          }
        }

        reordered.insert_val ( index_destination, repo );

        if ( reordered.length == repositories.length ) {
          this.config_file.set_repositories ( reordered );
        } else {
          ret = 1;
          stderr.printf ( "Failed to move '%s' repository to index %i\n", name, index_destination );
        }
      } else {
        ret = 1;
      }

      return ret;
    }

    private int set_option ( string key, string val ) {
      int ret = 0;

      if ( !this.config_file.set_option ( key, val ) ) {
        ret = 1;
        stderr.printf ( "Option '%s' does not exist\n", key );
      }

      return ret;
    }

    private int unset_option ( string key ) {
      int ret = 0;

      if ( !this.config_file.unset_option ( key ) ) {
        ret = 1;
        stderr.printf ( "Option '%s' does not exist\n", key );
      }

      return ret;
    }

    private void list_repository () {
      var repos = this.config_file.get_repositories ();

      var longest = this.repo_longest_name ( repos );

      for ( int i = 0; i < repos.length; i++ ) {
        var repo = repos.index ( i );
        string repo_line = repo.get_name ();
        if ( repo.get_commented () == Commented.YES ) {
          repo_line += this.build_separator ( repo_line.length, longest, 5 );
          repo_line += "(disabled)";
        }
        stdout.printf ( "%s\n", repo_line );
      }
    }

    private int repo_longest_name ( Array<Section> repos ) {
      Array<string> names = new Array<string>();
      for ( int i = 0; i < repos.length; i++ ) {
        names.append_val ( repos.index ( i ).get_name () );
      }

      return longest_string ( names );
    }

    private int longest_string ( Array<string> strings ) {
      int longest = 0;

      for ( int i = 0; i < strings.length; i++ ) {
        if ( strings.index ( i ).length > longest ) {
          longest = strings.index ( i ).length;
        }
      }

      return longest;
    }

    private string build_separator ( int initial, int longest, int space ) {
      string ret = "";
      int total = longest - initial + space;

      for ( int i = 0; i < total; i++ ) {
        ret += " ";
      }

      return ret;
    }

    private void list_options () {
      var options = this.config_file.get_options ();

      var longest = this.option_longest_key ( options );

      for ( int i = 0; i < options.length; i++ ) {
        var option = options.index ( i );
        string option_line = option.get_key ();
        if ( option.get_commented () == Commented.NO ) {
          if ( option.get_value () != "" ) {
            option_line += this.build_separator ( option_line.length, longest, 5 );
            option_line += option.get_value ();
          }
        } else {
          option_line += this.build_separator ( option_line.length, longest, 5 );
          option_line += "(no setted)";
        }
        stdout.printf ( "%s\n", option_line );
      }
    }

    private int option_longest_key ( Array<Key> options ) {
      Array<string> keys = new Array<string>();
      for ( int i = 0; i < options.length; i++ ) {
        keys.append_val ( options.index ( i ).get_key () );
      }

      return longest_string ( keys );
    }

    private bool repository_exists ( string name ) {
      bool ret = false;
      var repositories = this.config_file.get_repositories ();

      for ( int i = 0; i < repositories.length; i++ ) {
        if ( repositories.index ( i ).get_name () == name ) {
          ret = true;
          break;
        }
      }

      return ret;
    }

    private int repository_index ( string name ) {
      int ret = -1;
      var repositories = this.config_file.get_repositories ();

      for ( int i = 0; i < repositories.length; i++ ) {
        if ( repositories.index ( i ).get_name () == name ) {
          ret = i;
          break;
        }
      }

      return ret;
    }

    private string arg_to_key ( string arg ) {
      string ret = arg.replace ( "-", "" );

      if ( arg != "--siglevel" ) {
        var initial = ret.substring ( 0, 1 );
        var rest = ret.substring ( 1 );
        ret = initial.up () + rest;
      } else {
        ret = "SigLevel";
      }

      return ret;
    }

    private int save_file () {
      int ret = 0;

      this.config_file.set_repositories ( this.config_file.get_repositories () );

      if ( !FileHelper.write ( this.config_file.get_path (), this.config_file.to_string () ) ) {
        ret = 1;
      }

      return ret;
    }
  }
}
