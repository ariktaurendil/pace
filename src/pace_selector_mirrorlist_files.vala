/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_selector_mirrorlist_files.vala
 *
 * Copyright 2019 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Pace {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/pace/ui/pace_selector_mirrorlist_files.ui" )]
  public class SelectorMirrorlistFiles : Gtk.Grid, DataStatus {
    [GtkChild]
    private Gtk.Stack stack;
    [GtkChild]
    private Gtk.StackSidebar stack_sidebar;

    construct {
      this.change.connect ( this.page_needs_attention );
    }

    public void set_mirrorlist_files ( Array<Mirrorlist> mirrorlist_files ) {
      this.clear_stack ();
      for ( int i = 0; i < mirrorlist_files.length; i++ ) {
        var mirrorlist = mirrorlist_files.index ( i );

        var listbox = new ListBoxMirrors ( mirrorlist.get_path () );
        listbox.mirrors_changed.connect ( mirrorlist_changed );

        this.stack.add_titled ( listbox, "page_"+mirrorlist.get_file (), mirrorlist.get_file () );
      }

      if ( mirrorlist_files.length == 0 ) {
        this.set_visible ( false );
      }
    }

    public ConfigFile get_current_mirrorlist_file () {
      ListBoxMirrors listbox_current = this.stack.get_visible_child () as ListBoxMirrors;
      return listbox_current.get_mirrorlist_file ();
    }

    [GtkCallback]
    private void on_stack_add () {
      if ( this.stack.get_children ().length () == 1 ) {
        this.stack_sidebar.set_visible ( false );
      } else {
        this.stack_sidebar.set_visible ( true );
      }
    }

    public bool get_changed () {
      ListBoxMirrors listbox_current = this.stack.get_visible_child () as ListBoxMirrors;

      return listbox_current.get_changed ();
    }

    public string get_file_path () {
      return this.get_current_mirrorlist_file ().get_path ();
    }

    public void mirrorlist_changed () {
      this.change ();
    }

    private void clear_stack () {
      var children = this.stack.get_children ();

      for ( int i = 0; i < children.length (); i++ ) {
        var child = children.nth_data ( i );
        this.stack.remove ( child );
      }
    }

    private void page_needs_attention () {
      ListBoxMirrors listbox_current = this.stack.get_visible_child () as ListBoxMirrors;
      this.stack.child_set_property ( listbox_current as Gtk.Widget, "needs-attention", listbox_current.get_changed () );
    }
  }
}
