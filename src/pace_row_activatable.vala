/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_row_activatable.vala
 *
 * Copyright 2018-2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Pace {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/pace/ui/pace_row_activatable.ui" )]
  public class RowActivatable : Gtk.Grid {
    [GtkChild]
    private Gtk.Switch sw;
    public string switch_tooltip_text {
      set { this.sw.set_tooltip_text ( value ); }
    }

    public signal void changed ();

    construct {}

    public void on_row_activated () {
      this.sw.set_active ( !(this.sw.get_active () ) );
    }

    public bool get_enabled () {
      return this.sw.get_active ();
    }

    public void set_enabled ( bool enabled ) {
      this.sw.set_active ( enabled );
    }

    [GtkCallback]
    private void swtich_changed () {
      this.changed ();
    }
  }
}
