# Pace
A simple graphical tool to edit pacman.conf

![pace-1.4.1](https://gitlab.com/tarmaciltur/pace/raw/master/data/screenshot.png)

### Try Pace
**Dependencies**

```meson vala gtksourceview4```

**Build**
```sh
meson --prefix=/usr . build
ninja -C build
sudo ninja -C build install
```

-----

**Pace** is licensed under the GNU GPL v3+
